from django.core.exceptions import ValidationError
from django.forms import BaseInlineFormSet


class QuestionsInlineFormSet(BaseInlineFormSet):

    def clean(self):
        if not (self.instance.QUESTION_MIN_LIMIT <= len(self.forms) <= self.instance.QUESTION_MAX_LIMIT):
            raise ValidationError('Quantity of questions is out of range ({}..{})'.format(
                self.instance.QUESTION_MIN_LIMIT, self.instance.QUESTION_MAX_LIMIT
            ))

        order_nums = []

        for form in self.forms:
            order_nums.append(form.cleaned_data['number'])

        order_nums = sorted(order_nums)

        if not 1 in order_nums:
            raise ValidationError('Need question with number 1')

        for form in self.forms:
            if order_nums.count(form.cleaned_data['number']) > 1:
                raise ValidationError(f'Number {form.cleaned_data["number"]} is already used')

        for num in range(len(order_nums) - 1):
            if order_nums[num + 1] != num + 2:
                raise ValidationError('числа должны идти подряд по возрастанию')


class AnswerInlineFormSet(BaseInlineFormSet):

    def clean(self):
        if not (self.instance.ANSWER_MIN_LIMIT <= len(self.forms) <= self.instance.ANSWER_MAX_LIMIT):
            raise ValidationError('Quantity of answers is out of range ({}..{})'.format(
                self.instance.ANSWER_MIN_LIMIT, self.instance.ANSWER_MAX_LIMIT
            ))

        num_correct_answers = sum([
            1
            for form in self.forms
            if form.cleaned_data['is_correct']
        ])

        if num_correct_answers == 0:
            raise ValidationError('At LEAST one answer must be correct!')

        if num_correct_answers == len(self.forms):
            raise ValidationError('Not allowed to select ALL answers!')
