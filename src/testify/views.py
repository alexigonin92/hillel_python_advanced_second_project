from django.shortcuts import render
from django.views.generic import ListView

from accounts.models import User
from testify.models import Test


class TestListView(ListView):
    model = Test
    template_name = 'test-list.html'
    context_object_name = 'tests'


class UsersListView(ListView):
    model = User
    template_name = 'user-list.html'
    context_object_name = 'users'