from django.contrib import admin

from testify.forms import QuestionsInlineFormSet, AnswerInlineFormSet
from testify.models import Test, Question, Answer, TestResult, Topic


class AnswerInline(admin.TabularInline):
    model = Answer
    fields = ('text', 'is_correct')
    show_change_link = True
    formset = AnswerInlineFormSet
    extra = 0


class QuestionAdmin(admin.ModelAdmin):
    inlines = (AnswerInline,)


class QuestionsInline(admin.TabularInline):
    model = Question
    fields = ('text', 'number')
    show_change_link = True
    formset = QuestionsInlineFormSet
    extra = 0
    ordering = ('number',)


class TestAdmin(admin.ModelAdmin):
    inlines = (QuestionsInline,)


admin.site.register(Topic)
admin.site.register(Test, TestAdmin)
admin.site.register(Question, QuestionAdmin)
admin.site.register(Answer)
admin.site.register(TestResult)
