from django.urls import path

from testify.views import TestListView, UsersListView

app_name = 'tests'

urlpatterns = [
    path('', TestListView.as_view(), name='tests'),
    path('users/', UsersListView.as_view(), name='users'),
]
